console.log("Hello B248!");

/*

	Mini activity
	1. Create a function named greeting() and display the message you want to say to your say using console.log() inside of the function
	2. Invoke the greeting() 20x

*/

function greeting(){
	console.log("Hello Erwin, relax, take your time and eventually, kokopya ka na naman");
}


greeting();

//we can just use our loops

let countNum = 3;

while(countNum !==0){
	console.log("This is printed inside the loop " + countNum);
	greeting();
	countNum--;
}

//While Loop

/* 
	a while loop takes in an expression/condition
	- expressions are any unit of code that cna be evaluated to a value
	- if the condition evaluates to true, the statement inside the code block will be executed
	- a statement is a command that the programmer gives to a computer

	Loop will iterate a certain number of times until an expression/condition is met

	- iteration is the term given to the repetition of statements

	Syntax

	while(expression/condition){
		statement
	}

*/	

let count = 5;

while(count !== 0 ){
	//the current value of the count is also printed out
	console.log("While: " + count);

	//this decreases the value of count by 1 after every iteration to stop the loop when it reaches 0

	count--;

}

//do while loop

/*

	a do while loop works a lot like while loop, but unlike while loops, do-while loops guarantee that the code will be executed at least once

	Syntax
	do {
		statement
	} while (expression/condition);


*/	

let number =Number(prompt("Give me a number!"));

do {
	//the current value of number is printed out
	console.log("Do while " + number);
	//increases the value of number by 1 after every iteration to stop the loop when it reaches 10 or greater than
	//number = number + 1
	number += 1;

//Providing a number of 10 or greater will run the code block ONCE and will stop the loop	
} while(number < 10);


//For Loop

/*

- A for loop is more flexible than while and do-while loops. It consists of three parts:
	1. The "initialization" value that will track the progression of the loop.
	2. The "expreesion/condition" that will be evaluated which will determin whether the loop will run one more time.
	3. The "finalExpression" indicates how to advance the loop.

	-Syntax
	 for (initializationl; expression/condition; finalExpresion) {
		statement
	 }
*/

for (let count = 0; count <= 20; count++){
	console.log("For loop " + count);
}	 


for (let count = 0; count <= 20; count++){
	if(count % 2 === 0){
	console.log("Even " + count);
	}
}

let myString = "Vice Ganda"

console.log(myString.length);//

console.log(myString[0]);//V
console.log(myString[1]);//i
console.log(myString[2]);//c
console.log(myString[9]);//d


for(let x = 0; x<myString.length;x++){
	console.log(myString[x]);
}

let myName = "Zeref Dragneel";

for (let i=0; i < myName.length; i++){
	if(
		myName[i].toLowerCase() == "a" ||
		myName[i].toLowerCase() == "e" ||
		myName[i].toLowerCase() == "i" ||
		myName[i].toLowerCase() == "o" ||
		myName[i].toLowerCase() == "u"
		){

		console.log("Hi I'm a vowel!");

	}else{
		console.log(myName[i]);
	}
}

//Continue and Break Statemnts

/*
	The continue statement allows the code to go to the next iteration of the loop withouth finishing the execution of all statements in a code block

	The break statement is used to terminate the current loop once a match has been found
*/

for(let count = 0; count <=20; count++){

	if(count % 2 === 0){
		//Tells the code to continue to next iteration of the loop
		//This ignores all statements located after the continue statement
		continue;
	}

	console.log("Continue and Break:" + count);

	if(count>10){

		//tells the code to terminate/stop the loop even if the expression/condition of the loop defines that is should execute as long as the value of count is less then or equal to 20
		//the other number values will no longer be printed
		break;
	}
}

let name = "Mommy Dragneel Yonisia";

for(let i = 0; i < name.length; i++){
	console.log(name[i]);

	//if the vowel is equal to a, continue to the next iteration of the loop
	if(name[i].toLowerCase()==="a"){
		console.log("Continue to the next iteration");
		continue;
	}

	if(name[i] == "Y"){
		break;
	}

}

